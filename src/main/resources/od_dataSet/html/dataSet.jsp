<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="template" uri="http://www.jahia.org/tags/templateLib" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="jcr" uri="http://www.jahia.org/tags/jcr" %>
<%@ taglib prefix="ui" uri="http://www.jahia.org/tags/uiComponentsLib" %>
<%@ taglib prefix="functions" uri="http://www.jahia.org/tags/functions" %>
<%@ taglib prefix="query" uri="http://www.jahia.org/tags/queryLib" %>
<%@ taglib prefix="utility" uri="http://www.jahia.org/tags/utilityLib" %>
<%@ taglib prefix="s" uri="http://www.jahia.org/tags/search" %>
<%--@elvariable id="currentNode" type="org.jahia.services.content.JCRNodeWrapper"--%>
<%--@elvariable id="out" type="java.io.PrintWriter"--%>
<%--@elvariable id="script" type="org.jahia.services.render.scripting.Script"--%>
<%--@elvariable id="scriptInfo" type="java.lang.String"--%>
<%--@elvariable id="workspace" type="java.lang.String"--%>
<%--@elvariable id="renderContext" type="org.jahia.services.render.RenderContext"--%>
<%--@elvariable id="currentResource" type="org.jahia.services.render.Resource"--%>
<%--@elvariable id="url" type="org.jahia.services.render.URLGenerator"--%>
<div class="panel panel-primary">
      <div class="panel-heading">${currentNode.properties['jcr:title'].string}</div>
      <div class="panel-body">
<template:include view="shared"/>
</div>
<c:if test="${!renderContext.editMode}">
  TODO Add Internationalization
<table class="table table-striped">
  <thead>
    <th>Address</th>
    <th>City</th>
    <th>Country</th>
  </thead>
  <c:forEach items="${jcr:getChildrenOfType(currentNode, 'od:dataEntry')}" var="dataEntry" varStatus="status">
  <template:module node="${dataEntry}" view="line"/>
 </c:forEach>
  </table>
  </c:if>
  <c:if test="${renderContext.editMode}">
<c:forEach items="${jcr:getChildrenOfType(currentNode, 'od:dataEntry')}" var="dataEntry" varStatus="status">
  <template:module node="${dataEntry}" view="default"/>
 </c:forEach>
  </c:if>
 
</div>
<c:if test="${renderContext.editMode}">
  <template:module path="*"/>
</c:if>
TODO add downloads options